package sshen.funweather.adapter;

import java.util.ArrayList;

import sshen.funweather.model.NavDrawerItem;

import sshen.funweather.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class NavDrawerListAdapter extends BaseAdapter {
     
    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
     
    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }
 
    @Override
    public int getCount() {
        return navDrawerItems.size();
    }
 
    @Override
    public Object getItem(int position) {       
        return navDrawerItems.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
          
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView count = (TextView) convertView.findViewById(R.id.counter);
        
        
        // display image icon
        if (position > 1) {
        	imgIcon.setImageResource(R.drawable.ic_red_pin_64);
        }
        else {
        	imgIcon.setImageResource(navDrawerItems.get(position).getIcon());        
        }
          
        // display title
        title.setText(navDrawerItems.get(position).getTitle());
         
        // displaying count, check whether it set visible or not
        if(navDrawerItems.get(position).getCounterVisibility()){
            count.setText(navDrawerItems.get(position).getCount());
        }else{
            // hide the counter view
            count.setVisibility(View.GONE);
        }
         
        return convertView;
    }
 
}
