package sshen.funweather.adapter;

import java.util.ArrayList;

import sshen.funweather.R;
import sshen.funweather.model.ForecastGridItem;
import sshen.funweather.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ForecastGridAdapter extends BaseAdapter{
	
	private Context context;
	private ArrayList<ForecastGridItem> gridItems;
	
	public ForecastGridAdapter(Context context, ArrayList<ForecastGridItem> list) {
		this.context = context;
		gridItems = list;
	}

	@Override
	public int getCount() {
		return gridItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.forecast_grid_item, null);
		}
		
		// initialize UI
		TextView dateField = (TextView) convertView.findViewById(R.id.label_date);
		TextView weatherIcon = (TextView) convertView.findViewById(R.id.forecast_icon);
		weatherIcon.setTypeface(Utils.getWeatherFont(context));
		TextView descriptionField = (TextView) convertView.findViewById(R.id.label_forecast_description);
		TextView detailsField = (TextView) convertView.findViewById(R.id.label_forecast_details);
		
		// set values
		ForecastGridItem item = gridItems.get(position);
		dateField.setText(item.getDate());
		descriptionField.setText(item.getDescription());
		detailsField.setText(item.getDetails());
		weatherIcon.setText(item.getIcon());
		
		
		return convertView;
	}

}
