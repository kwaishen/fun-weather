package sshen.funweather.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import android.app.Activity;

public class CityPreference {
	
	public static final String DEFAULT_CITY = "Vancouver,CA";
	public static final City DEFAULT_CITY_OBJ = new City(6173331,"Vancouver", DEFAULT_CITY);
	public static Hashtable<Integer, City> table = new Hashtable<Integer, City>();
	public static ArrayList<City> list = new ArrayList<City>();
	
	
	public static String getCity(Activity activity) {
		return activity.getPreferences(Activity.MODE_PRIVATE).getString("city", DEFAULT_CITY);
	}

	public static void setCity(Activity activity, City city) {
		activity.getPreferences(Activity.MODE_PRIVATE).edit().putString("city", city.getFullName()).commit();
		saveCity(city);
	}
	
	public static void saveCity(City city) {
		if (!isDuplicate(city.getId())) {
			table.put(city.getId(), city);
			list.add(city);
		} else {
//			System.out.println("found a dup city, won't add to table!");
		}
	}
	
	public static boolean isDuplicate(int cityId) {
		return table.containsKey(cityId);
	}
	
	public static List<City> getSavedList() {
		return list;
	}
	
}
