package sshen.funweather.utils;

import android.app.Activity;

public class UserPreference {
	
	public static final String DEGREE_CELSIUS = "metric";
	public static final String DEGREE_FAHRENHEIT = "imperial";
	
	public static String getDefaultDegree(Activity activity) {
		return activity.getPreferences(Activity.MODE_PRIVATE).getString("degree", DEGREE_CELSIUS);
	}
	
	public static void setDefaultDegree(Activity activity, boolean isCelsius) {
		activity.getPreferences(Activity.MODE_PRIVATE).edit().putString("degree", isCelsius ? DEGREE_CELSIUS : DEGREE_FAHRENHEIT).commit();	
	}
	
	/**
	 * Check degree preference
	 * @param activity
	 * @return true if celsius, false otherwise
	 */
	public static boolean isCelsius(Activity activity) {
		return getDefaultDegree(activity).equals(DEGREE_CELSIUS);
	}
	
	/**
	 * Store users last viewed index in drawers
	 * @param activity
	 * @param index
	 */
	public static void setLastViewedIndex(Activity activity, int index) {
		activity.getPreferences(Activity.MODE_PRIVATE).edit().putInt("lastVisitedIndex", index).commit();
	}
	
	public static int getLastViewIndex(Activity activity) {
		return activity.getPreferences(Activity.MODE_PRIVATE).getInt("lastVisitedIndex", 1);
	}

}
