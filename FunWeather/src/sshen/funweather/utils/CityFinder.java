package sshen.funweather.utils;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

public class CityFinder {
	
	/**
	 * Helper method, converts json object to city
	 * @param json
	 * @return
	 */
	public static City toCity(JSONObject json) {
		if (json == null) return null;
		
		try {
			JSONObject item = json.getJSONArray("list").getJSONObject(0);
			int id = item.getInt("id");
			String name = item.getString("name").toUpperCase(Locale.US);
			String country = item.getJSONObject("sys").getString("country");
			String fullName = Utils.capitalize(name.toLowerCase(Locale.US) + "," + country);
			
			City city = new City(id, name, fullName);
			return city;
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
