package sshen.funweather.utils;

public class City {
	
	private int id;
	private String cityName;
	private String fullName;
	
	public City(int id, String cityName, String fullName) {
		this.id = id;
		this.cityName = cityName;
		this.fullName = fullName;
	}
	
	public City() {
		this.id = 0;
		this.cityName = "";
		this.fullName = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int mId) {
		this.id = mId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String mCityName) {
		this.cityName = mCityName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return fullName;
	}

}
