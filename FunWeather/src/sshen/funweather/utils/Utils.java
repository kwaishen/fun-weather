package sshen.funweather.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import sshen.funweather.R;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

public class Utils {
	
	public static final String TAG = "Utils";
	public static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
			"E d/MM", Locale.getDefault());
	public static SimpleDateFormat HOUR_FORMATTER = new SimpleDateFormat(
			"HH:mm", Locale.getDefault());
	
	public static final String FORECAST_CACHE_KEY = "_forecast";
	public static final String CURRENT_CACHE_KEY = "_current";
	public static final String WEATHER_FONT_PATH = "fonts/weatherfont.ttf";
	public static Typeface weatherFont;

	/* Wind for angles */
	public static String[] winds = new String[] {"N","NE","E","SE","S",
		"SW","W","NW","N"};
	
	public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	
	public static boolean isStringValid(String val) {
		if (val != null && !"".equals( val.trim() )) {
			return true;
		}
		return false;
	}
	
	/**
	 * Hide virtual keyboard
	 * @param activity
	 */
	public static void dismissKeyBoard(Activity activity) {
		InputMethodManager inputManager = (InputMethodManager)
				activity.getSystemService(Context.INPUT_METHOD_SERVICE); 
		
		inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	/**
	 * Capitalize 1st word of a string
	 * @param original input string
	 * @return string with 1st word capitalized
	 */
	public static String capitalize(String original) {
		if (original.length() == 0) {
			return original;
		}
		return original.substring(0, 1).toUpperCase(Locale.US) + original.substring(1);
	}
	
	public static Typeface getWeatherFont(Context context) {
		if (weatherFont == null) {
			weatherFont = Typeface.createFromAsset(context.getAssets(), WEATHER_FONT_PATH);
		}
		
		return weatherFont;
	}
	
	/**
	 * Quicker way to convert weather id to weather icon
	 * @param context
	 * @param realdId
	 * @return
	 */
	public static int getSimpleWeatherIcon(Context context, int realdId) {
		int icon = R.string.weather_unknown;
		
		int simpleId = realdId / 100;

		switch(simpleId) {
		case 2:
			icon = R.string.weather_thunderstorm;
			break;
		case 3:
			icon = R.string.weather_dizzle;
			break;
		case 5:
			icon = R.string.weather_rainny;
			break;
		case 6:
			icon = R.string.weather_snowy;
			break;
		case 7:
			icon = R.string.weather_foggy;
			break;
		case 8:
			icon = R.string.weather_sunny_day;
			break;
		case 9:
			icon = R.string.weather_tornado;
		default:
			icon = R.string.weather_cloudy;
		}
				
		return icon;
	}
	
	/**
	 * Convert weather code to icon
	 * @param sunset
	 * @return
	 */
	public static String getWeatherIcon(Context context, int realdId, long sunrise, long sunset) {
		int icon = R.string.weather_unknown;
		long currentTime = new Date().getTime();
		boolean isDay = (currentTime >= sunrise && currentTime < sunset) 
				|| (sunrise == -1 && sunset == -1);
		
		switch(realdId) {
		
		case 200:
		case 210:
		case 230:
		case 231:
			icon = R.string.weather_storm_shower;
			break;
		
		case 511:
			icon = R.string.weather_snowy; 
			break;
		case 520:
			icon = R.string.weather_showers;
			break;
			
		case 800:
			icon = isDay ? R.string.weather_sunny_day : R.string.weather_sunny_night;
			break;
		case 801:
			icon = isDay ? R.string.weather_cloudy_day : R.string.weather_cloudy_night;
			break;
		case 802:
		case 803:
			icon = R.string.weather_cloudy;
			break;
			
		case 781 :
		case 900 :
			icon = R.string.weather_tornado;
			break;
		case 901:
			icon = R.string.weather_thunderstorm;
			break;
		case 905:
			icon = R.string.weather_extreme_wind;
			break;
		default:
			icon = getSimpleWeatherIcon(context, realdId);
			break;
				
		}
		return context.getString(icon);
	}
	
	// not get called for now
	public static String getDirectoinFromAngle(float angle) {
		int index = 0;
		int temp = Math.round(angle / 45f );
		
		if (temp >= 0 && temp < winds.length) {
			index = temp;
		}
		return winds[index];
	}
	
	public static boolean isConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}
	
	public static String getLatLng(Context context, Location currentLocation) {
		if (currentLocation != null) {
			return context.getString(R.string.latitude_longitude,
					currentLocation.getLatitude(),
					currentLocation.getLongitude());
		}
		return "";
	}
	
	// not get called for now
	public static void saveInPreferences(Context context, String property, int value) {
		if (context != null && property != null) {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putInt(property, value);
			editor.commit();
		}
	}
	
	// not get called for now
	public static int getPreferenceValue(String key, int defaultValue, Context context) {
		int value = defaultValue;
		
		if (key != null && context != null) {
			SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
			value = sharedPrefs.getInt(key, defaultValue);
		}
		return value;
	}
	
	// not get called for now
	public static String getPreferenceValue(String key, String defaultValue, Context context) {
		String value = defaultValue;
		
		if (key != null && context != null) {
			SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
			value = sharedPrefs.getString(key, defaultValue);
		}
		return value;
	}
	
	/**
	 * convert List to array string
	 * @param list List
	 * @return string array
	 */
	public static <T> String[] toArray(List<T> list) {
		String[] arr = new String[list.size()];
		for (int i=0; i<list.size(); i++) {
			arr[i] = list.get(i).toString();
		}
		return arr;
	}
	
	/**
	 * convert string to json object
	 * @param json
	 * @return
	 */
	public static JSONObject toJSONObject(String json) {
		JSONObject data = null;
		if (!isStringValid(json))
			return null;
		
		try {
			data = new JSONObject(json);
		} catch (JSONException e) {
			data = null;
			e.printStackTrace();
		}
		return data;
	}
	
	/**
	 * Validate json response
	 * @param json
	 * @return
	 */
	public static JSONObject validateResponse(String json){
		JSONObject data = null;
		if (!isStringValid(json))
			return null;
		
		try {
			data = new JSONObject(json);
			// 200 means response OK, otherwise error
			if (data != null && data.getInt("cod") != 200) {
				data = null;
			}
		} catch (JSONException e) {
			data = null;
			e.printStackTrace();
		}
		return data;
	}


}
