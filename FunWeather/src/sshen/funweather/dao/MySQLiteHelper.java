package sshen.funweather.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MySQLiteHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "cities.db";
	private static final int DATABASE_VERSION = 1;

	// city fields
	public static final String TABLE_CITY = "comments";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_CITY_ID = "cityId";
	public static final String COLUMN_CITY_NAME = "name";
	public static final String COLUMN_FULL_NAME = "fullName";
	
	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_CITY + "(" + COLUMN_ID
			+ " integer not null primary key autoincrement, " + COLUMN_CITY_NAME
			+ " varchar not null, " + COLUMN_FULL_NAME
			+ " varchar not null, " + COLUMN_CITY_ID
			+ " integer not null);";
	
	
	public MySQLiteHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//		Log.w(MySQLiteHelper.class.getName(),
		//		        "Upgrading database from version " + oldVersion + " to "
		//		            + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITY);
		onCreate(db);
	}

}
