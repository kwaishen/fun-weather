package sshen.funweather.dao;

import java.util.ArrayList;
import java.util.List;

import sshen.funweather.utils.City;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CityDao {
	
	// Database fields
	  private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	  private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
			  MySQLiteHelper.COLUMN_CITY_NAME, MySQLiteHelper.COLUMN_FULL_NAME, MySQLiteHelper.COLUMN_CITY_ID};
	  
	  public CityDao(Context context) {
		  dbHelper = new MySQLiteHelper(context);
	  }
	  
	  public void open() throws SQLException {
		  database = dbHelper.getWritableDatabase();
	  }

	  public void close() {
		  dbHelper.close();
	  }
	  
	  /**
	   * Insert a new city to city table
	   * @param city City
	   * @return record id
	   */
	  public boolean createCity(City city) {
		  ContentValues values = new ContentValues();
		  values.put(MySQLiteHelper.COLUMN_CITY_NAME, city.getCityName());
		  values.put(MySQLiteHelper.COLUMN_FULL_NAME, city.getFullName());
		  values.put(MySQLiteHelper.COLUMN_CITY_ID, city.getId());
		  
		  long insertId = database.insert(MySQLiteHelper.TABLE_CITY, null,values);
		  return insertId != -1;
	  }
	  
	  /**
	   * Retrieve all cities
	   * @return a list of cities
	   */
	  public List<City> getAllCities() {
		  List<City> cities = new ArrayList<City>();
		  Cursor cursor = database.query(MySQLiteHelper.TABLE_CITY, allColumns, null, null, null, null, null);
		  cursor.moveToFirst();
		  while (!cursor.isAfterLast()) {
			  City city = cursorToCity(cursor);
			  cities.add(city);
			  cursor.moveToNext();
		  }
		  // make sure to close the cursor
		  cursor.close();
		  return cities;
	  }
	  
	  /**
	   * Convert cursor to city
	   * @param cursor
	   * @return
	   */
	  private City cursorToCity(Cursor cursor) {
		  City city = new City();
		  city.setCityName(cursor.getString(1));
		  city.setFullName(cursor.getString(2));
		  city.setId(cursor.getInt(3));
		  return city;
	  }

}
