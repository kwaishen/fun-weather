package sshen.funweather.model;

public class ForecastGridItem {
	
	private String date;
	private String details;
	private String description;
	private String icon;
	
	public ForecastGridItem(String date, String desc, String details, String icon) {
		this.date = date;
		this.description = desc;
		this.details = details;
		this.icon = icon;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
}
