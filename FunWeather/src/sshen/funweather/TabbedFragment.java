package sshen.funweather;

import sshen.funweather.fragment.CurrentWeatherFragment;
import sshen.funweather.fragment.WeatherForecastFragment;
import sshen.funweather.utils.City;
import sshen.funweather.utils.Utils;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class TabbedFragment extends Fragment{
	
	private static final String TAG = "TabbedFragment";
	private Location mLocation;
	private City mCity;
	private WeatherPagerAdapter mWeatherPagerAdapter;
	private ViewPager mViewPager;
	private CurrentWeatherFragment mCurrentWeatherFragment;
	private WeatherForecastFragment mWeatherForecastFragment;
	
	public TabbedFragment(Location location) {
		mLocation = location;
	}
	
	public TabbedFragment(City city) {
		mCity = city;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// initialize UI
		View v = inflater.inflate(R.layout.fragment_tabbed, container, false);
		mWeatherPagerAdapter = new WeatherPagerAdapter(getChildFragmentManager()); 
		mViewPager = (ViewPager) v.findViewById(R.id.pager);
		mViewPager.setAdapter(mWeatherPagerAdapter);
		
		return v;
	}
	
	// getters
	public City getCity() {
		return mCity;
	}
	
	public Location getLocation() {
		return mLocation;
	}
	
	public CurrentWeatherFragment getCurrentWeatherFragment() {
		if (mCurrentWeatherFragment == null) {
			if (mCity != null) {
				mCurrentWeatherFragment = new CurrentWeatherFragment(mCity);
			}
			else {
				mCurrentWeatherFragment = new CurrentWeatherFragment(mLocation);
			}
		}
		return mCurrentWeatherFragment;
	}

	
	public WeatherForecastFragment getWeatherForecastFragment() {
		if (mWeatherForecastFragment == null) {
			if (mLocation != null) {
				mWeatherForecastFragment = new WeatherForecastFragment(mLocation);
			}
			else {
				mWeatherForecastFragment = new WeatherForecastFragment(mCity);
			}
		}
		
		return mWeatherForecastFragment;
	}
	
	
	/**
	 * Force refresh current weather and weather forecast
	 */
	public void refresh() {
		boolean hasNetwork = Utils.isConnected(getActivity());
		if (!hasNetwork) {
			Toast.makeText(getActivity(), R.string.no_network, Toast.LENGTH_SHORT).show();
			return;
		}
		
		// update current weather and weather forecast concurrently
		new Handler().post(new Runnable(){
			@Override
			public void run() {
				mCurrentWeatherFragment.refresh();
			}});
		
		new Handler().post(new Runnable(){
			@Override
			public void run() {
				mWeatherForecastFragment.refresh();
			}});
		
	}
	
	
	/**
	 * Pager Adapter Class
	 * @author Steve
	 *
	 */
	public class WeatherPagerAdapter extends FragmentPagerAdapter {

		public WeatherPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch(position) {
			case 0: 
				return getCurrentWeatherFragment();
			case 1:
				return getWeatherForecastFragment();
			}
			return null;
		}

		@Override
		public int getCount() {
			return 2;
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.current_weather);
			case 1:
				return getString(R.string.weather_forecast);
			}
			return null;
			
		}
	}

}
