package sshen.funweather.dialogs;

import sshen.funweather.R;
import sshen.funweather.interfaces.Communicator;
import sshen.funweather.utils.UserPreference;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SettingsDialogFragment extends DialogFragment{
	
	private RadioButton radioCelsius;
	private RadioButton radioFahrenheit;
	private RadioGroup radioGroup;
	private Communicator communicator;
	private boolean isCelsius;
	private boolean isLoaded;
	
	public static SettingsDialogFragment newInstance() {
		SettingsDialogFragment dialog = new SettingsDialogFragment();
		return dialog;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		isCelsius = UserPreference.isCelsius(getActivity());
		isLoaded = false;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// Initialize UI
		getDialog().setTitle(R.string.title_settings);
		View v = inflater.inflate(R.layout.dialog_settings, container, false);
		radioGroup = (RadioGroup) v.findViewById(R.id.degreeGroup);
		radioGroup.setOnCheckedChangeListener(changedListener);
		
		radioCelsius = (RadioButton) v.findViewById(R.id.radio_celsius);
		radioFahrenheit = (RadioButton) v.findViewById(R.id.radio_fahrenheit);
		
		radioCelsius.setChecked(isCelsius);
		radioFahrenheit.setChecked(!isCelsius);
		
		radioCelsius.setOnClickListener(clickListener);
		radioFahrenheit.setOnClickListener(clickListener);
		
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		communicator = (Communicator) activity;
	}
	
	private OnCheckedChangeListener changedListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// do nothing if the fragment is loaded the 1st time
			if (!isLoaded) {
				isLoaded = true;
				return;
			}
			
			switch (checkedId) {
			case R.id.radio_celsius :
				isCelsius = true;
				break;
			case R.id.radio_fahrenheit :
				isCelsius = false;
				break;
			default:
				return;
			}
			
			// store preference
			UserPreference.setDefaultDegree(getActivity(), isCelsius);
			// force refresh weather info
			communicator.refresh();
		}
	};
	
	private OnClickListener clickListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			getDialog().dismiss();
		}
	};
	

}
