package sshen.funweather;

import java.util.ArrayList;

import sshen.funweather.adapter.NavDrawerListAdapter;
import sshen.funweather.dao.CityDao;
import sshen.funweather.dialogs.SettingsDialogFragment;
import sshen.funweather.fragment.EditCityFragment;
import sshen.funweather.interfaces.Communicator;
import sshen.funweather.model.NavDrawerItem;
import sshen.funweather.utils.City;
import sshen.funweather.utils.CityPreference;
import sshen.funweather.utils.LocationGetter;
import sshen.funweather.utils.LocationGetter.LocationResult;
import sshen.funweather.utils.UserPreference;
import sshen.funweather.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;



public class MainActivity extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener, 
															  LocationListener, Communicator{
	private static final int ONE_MINUTE = 60 * 1000;
	
	// Google Play Services
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    
    // Tags
    private static final String TAG = "MainActivity";
    private static final String TAG_GOOGLE = "GoogleAPIClient";
    
    private TabbedFragment mTabbedFragment;
    
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
 
    // nav drawer title
    private CharSequence mDrawerTitle;
 
    // used to store app title
    private CharSequence mTitle;
 
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
 
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter drawerAdapter;
    
    // SQLite
    private CityDao cityDao;
    private boolean doubleBackButtonPressed;
    
    // helper variables
    private boolean isAppFirstOpened;
    
    

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // setting helper variables when the app is lauched at the 1st time
        if (savedInstanceState == null) {
        	doubleBackButtonPressed = false;
        	isAppFirstOpened = true;
        	cityDao = new CityDao(this);
        	cityDao.open();
        }
        else {
        	isAppFirstOpened = false;
        }
        
        mTitle = mDrawerTitle = getTitle();
 
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
 
        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
 
        // populate Drawer list
        navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        for (City city : cityDao.getAllCities()) {
        	navDrawerItems.add(new NavDrawerItem(city.getCityName(), 0, city));
        }
        
        // Recycle the typed array
        navMenuIcons.recycle();
 
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
 
        // setting the nav drawer list adapter
        drawerAdapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(drawerAdapter);
 
        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        
        // the java way of hiding app icon
//        getActionBar().setDisplayUseLogoEnabled(false);
//        getActionBar().setDisplayShowHomeEnabled(false);
        
        
        /* following code is using support.v7*/
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name) {
        	public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }
 
            public void onDrawerOpened(View drawerView) {
            	Utils.dismissKeyBoard(MainActivity.this);
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        // setting up Google Play Services
        buildGoogleApiClient();
        createLocationRequest();
    }
    
    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        	navigateTo(position);
        	new Handler().post(new Runnable() {
        		@Override
        		public void run() {
        			UserPreference.setLastViewedIndex(MainActivity.this, position);
        		}
        	});
        }
    }
    
    protected synchronized void buildGoogleApiClient() {
    	mGoogleApiClient = new GoogleApiClient.Builder(this)
    		.addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
    }
    
    protected void createLocationRequest() {
    	mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(ONE_MINUTE);
        mLocationRequest.setFastestInterval(ONE_MINUTE/2);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    
    protected void startLocationUpdates() {
    	LocationServices.FusedLocationApi.requestLocationUpdates(
    			mGoogleApiClient, mLocationRequest, this);
    }
    
    protected void stopLocationUpdates() {
    	LocationServices.FusedLocationApi.removeLocationUpdates(
    			mGoogleApiClient, this);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	switch (requestCode) {
		case Utils.CONNECTION_FAILURE_RESOLUTION_REQUEST:
			switch (resultCode) {
			case Activity.RESULT_OK:
				break;
			default: // Disconnect
				break;
			}
			break;

		default:
			// Unknown request code
			break;
		}
    }
    
    
	@Override
	public void onConnected(Bundle arg0) {
		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null) {
//            Log.d(TAG_GOOGLE, String.valueOf(mLastLocation.getLatitude() + ", " + String.valueOf(mLastLocation.getLongitude())));
            if (isAppFirstOpened) {
            	navigateTo(1);
            }
        }
	}

	
	@Override
	public void onLocationChanged(Location location) {
		mCurrentLocation = location;
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (result.hasResolution()) {
			try {
				result.startResolutionForResult(this, Utils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
			} catch (Exception e) {
				// no resolution ==> manage error message
			}
		}
	}
	
	
	
	private void startLocation() {
		if (servicesConnect()) {
			Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
			
			if (location != null) {
				Toast.makeText(this, R.string.new_location_set, Toast.LENGTH_SHORT).show();
				
				if (mTabbedFragment != null) {
//					mTabbedFragment.loadLocation(location);
				}
			} else {
				detectLocationWithoutPlayServices();
			}
		} else {
			detectLocationWithoutPlayServices();
		}
	}
	
	private boolean servicesConnect() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		return ConnectionResult.SUCCESS == resultCode;
	}
	
	private void detectLocationWithoutPlayServices() {
		LocationResult locationResult = new LocationResult() {

			@Override
			public void gotLocation(Location location) {
				// TODO Auto-generated method stub
				if (location != null) {
					Toast.makeText(MainActivity.this, R.string.new_location_set, Toast.LENGTH_SHORT).show();
					
					if (mTabbedFragment != null) {
//						mTabbedFragment.loadLocation(location);
					}
				} else {
					Toast.makeText(MainActivity.this, R.string.error_gps, Toast.LENGTH_SHORT).show();
				}
				
			}
			
		};
		
		LocationGetter userLocation = new LocationGetter();
		boolean locationEnabled = userLocation.getLocation(this, locationResult);
		
		if (!locationEnabled) {
			Toast.makeText(MainActivity.this, R.string.enbale_gps, Toast.LENGTH_SHORT).show();
		}
	}
    
    @Override
    public void onBackPressed() {
    	if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
    		mDrawerLayout.closeDrawer(mDrawerList);
    	} else {
    		if (doubleBackButtonPressed) {
    			super.onBackPressed();
    			return;
    		}
    		
    		doubleBackButtonPressed = true;
    		Toast.makeText(this, R.string.press_back_to_exit, Toast.LENGTH_SHORT).show();
    		
    		new Handler().postDelayed(new Runnable() {

    	        @Override
    	        public void run() {
    	        	doubleBackButtonPressed = false;                       
    	        }
    	    }, 2000); // 2 seconds
    		
    	}
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
        mGoogleApiClient.connect();
    }
    
    @Override
    protected void onResume() {
    	cityDao.open();
    	super.onResume();
    	drawerAdapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(drawerAdapter);
        
        if (mGoogleApiClient.isConnected() ) {
        	startLocationUpdates();
        }
        
    }
    
    @Override
    protected void onPause() {
    	cityDao.close();
    	super.onPause();
    	if (mGoogleApiClient.isConnected() ) {
    		stopLocationUpdates();
        }
    	
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	if (mGoogleApiClient.isConnected() ) {
    		mGoogleApiClient.disconnect();
        }
    }
    
    @Override
    protected void onRestart() {
    	super.onRestart();
    	isAppFirstOpened = false;
    }
    

    
    
    
    /**
     * Displaying fragment view for selected nav drawer list item
     * */
    private void navigateTo(int position) {
        // update the main content by replacing fragments
    	Fragment fragment = null;
    	
    	if (position == 0) {
    		fragment = new EditCityFragment();
    	}
    	else if (position == 1){
    		if (mLastLocation != null) {
    			fragment = new TabbedFragment(mLastLocation);
    		} else {
    			fragment = new TabbedFragment(CityPreference.DEFAULT_CITY_OBJ);
    		}
    		mTabbedFragment = (TabbedFragment) fragment;
    	}
    	else {
    		City city = navDrawerItems.get(position).getCity();
    		fragment = new TabbedFragment(city);
    		mTabbedFragment = (TabbedFragment) fragment;
    	}
    	
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
 
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            
            // set drawer title
            if (position < 2) {
            	setTitle(navMenuTitles[position]);
            }
            else {
            	setTitle(mTabbedFragment.getCity().getFullName());
            }
            
            mDrawerLayout.closeDrawer(mDrawerList);
        }
        
    }
    
    /**
     * Set app title and action bar title
     */
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
    
    
    /**
     * Add a new drawer item at the end of drawer list 
     */
    @Override
    public void appendToListView(City city) {
    	if (mTabbedFragment != null) {
    		NavDrawerItem newItem = new NavDrawerItem(city.getCityName(), 0 ,city);
    		navDrawerItems.add(newItem);
    		drawerAdapter.notifyDataSetChanged();
    	}
    	
    }
    
    /**
     * Force refresh current weather and weather forecast
     */
    @Override
    public void refresh() {
    	if (mTabbedFragment != null) {
    		mTabbedFragment.refresh();
    	}
    	else {
    		Toast.makeText(getApplicationContext(), R.string.error_refresh, Toast.LENGTH_SHORT).show();;
    	}
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, disable the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setEnabled(!drawerOpen);
        menu.findItem(R.id.action_refresh).setEnabled(!drawerOpen);
        menu.findItem(R.id.action_gps).setEnabled(!drawerOpen);
    	
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
    		mDrawerLayout.closeDrawer(mDrawerList);
    		return true;
    	}
        
        // Handle action bar actions click
        switch (item.getItemId()) {
        case R.id.action_refresh:
        	refresh();
        	return true;
        case R.id.action_gps:
        	//startLocation();
    		if (mLastLocation != null) {
    			Toast.makeText(getApplicationContext(), 
    					String.valueOf(mLastLocation.getLatitude() + ", " + String.valueOf(mLastLocation.getLongitude())), 
    					Toast.LENGTH_SHORT).show();
            }
        	return true;
        case R.id.action_settings:
        	loadDialogFragment(SettingsDialogFragment.newInstance(), "settingsDialog");
            return true;
        
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    /**
     * Create and pop dialog fragment
     * @param df dialog fragment object
     * @param tag given tag
     * @return
     */
    private int loadDialogFragment(DialogFragment df, String tag) {
    	FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
	    Fragment prev = getSupportFragmentManager().findFragmentByTag(tag);
	    if (prev != null) {
	        ft.remove(prev);
	    }
		
	    ft.addToBackStack(null);
	    
	    return df.show(ft, "settingsDialog");
    }
    
    
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


}
