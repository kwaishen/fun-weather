package sshen.funweather.fragment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import sshen.funweather.MainActivity;
import sshen.funweather.R;
import sshen.funweather.cache.RawCache;
import sshen.funweather.grabber.WeatherGrabber;
import sshen.funweather.utils.City;
import sshen.funweather.utils.UserPreference;
import sshen.funweather.utils.Utils;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


public class CurrentWeatherFragment extends Fragment{
	
	private static final String TAG = "CurrentWeatherFragment";
	private Calendar c = Calendar.getInstance();
	
	private TextView refreshField, refreshIcon, cityField, updateField, detailsFields,
			currentTemperatureField, weatherIcon;
	private Handler handler;
	
	private City city;
	
	private Location location;
	
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public CurrentWeatherFragment(City city) {
		this.city = city;
	}
	
	public CurrentWeatherFragment(Location location) {
		this.location = location;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_current_weather, container, false);
		
		// Initialize UI
		refreshField = (TextView) rootView.findViewById(R.id.label_refreshing);
		refreshIcon = (TextView) rootView.findViewById(R.id.weather_refresh_icon);
		cityField = (TextView) rootView.findViewById(R.id.label_city);
		updateField = (TextView) rootView.findViewById(R.id.label_update);
		detailsFields = (TextView) rootView.findViewById(R.id.label_details);
		currentTemperatureField = (TextView) rootView.findViewById(R.id.label_current_temperature);
		weatherIcon = (TextView) rootView.findViewById(R.id.weather_icon);
		weatherIcon.setTypeface(Utils.getWeatherFont(getActivity()));
		refreshIcon.setTypeface(Utils.getWeatherFont(getActivity()));
		refreshIcon.setText(getString(R.string.weather_refresh));
		
		return rootView;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handler = new Handler();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "start onResume");
		
		if (city != null) {
			loadCityWeather(city.getFullName());
		} else if (location != null) {
			loadLocationWeather(location);
		} else {
			Log.e(TAG, "ERROR onResume, so load default city");
		}
		
	}
	
	
	private void stopRefresh() {
		refreshField.setVisibility(View.GONE);
		refreshIcon.setVisibility(View.GONE);
		cityField.setVisibility(View.VISIBLE);
		updateField.setVisibility(View.VISIBLE);
		detailsFields.setVisibility(View.VISIBLE);
		currentTemperatureField.setVisibility(View.VISIBLE);
		weatherIcon.setVisibility(View.VISIBLE);
	}
	
	private void startRefresh() {
		refreshField.setVisibility(View.VISIBLE);
		refreshIcon.setVisibility(View.VISIBLE);
		cityField.setVisibility(View.GONE);
		updateField.setVisibility(View.GONE);
		detailsFields.setVisibility(View.GONE);
		currentTemperatureField.setVisibility(View.GONE);
		weatherIcon.setVisibility(View.GONE);
		
	}
	
	public void refresh() {
		if (this.city != null) {
			updateWeatherCity(city.getFullName(), true);
		}
		else if (this.location != null) {
			updateWeatherLocation(location, true);
		}
		else {
			Toast.makeText(getActivity(), R.string.error_refresh_city, Toast.LENGTH_SHORT).show();
		}
		
	}
	
	private void loadCityWeather(String city) {
		updateWeatherCity(city, false);
		
	}
	
	/**
	 * Update weather information
	 * @param city City
	 * @param force force update or not
	 */
	private void updateWeatherCity(final String city, boolean force) {
		startRefresh();
		
		// check from cache
		boolean isInCache = RawCache.isInCache(getActivity(), city + Utils.CURRENT_CACHE_KEY);
		boolean hasNetwork = Utils.isConnected(getActivity());
		
		// no network and no cache --> error
		if (!hasNetwork && !isInCache) {
			Toast.makeText(getActivity(), R.string.no_network, Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (isInCache && !force) {
			String json = RawCache.fetchFromCache(getActivity(), city + Utils.CURRENT_CACHE_KEY);
			stopRefresh();
			
			JSONObject data = Utils.toJSONObject(json);
			if (data == null) {
				Toast.makeText(getActivity(), "Current Weather "+ getString(R.string.error_parssing), Toast.LENGTH_SHORT).show();
				return;
			}
			
			renderWeather(data);
			
		} else {
			// get from network in separate Thread
			new Thread() {
				public void run() {
					final String temp = WeatherGrabber.loadCurrentWeather(getActivity(), city, UserPreference.getDefaultDegree(getActivity()));
					JSONObject data = Utils.validateResponse(temp);
					
					if (data == null) {
						// error finding city
						handler.post(new Runnable() {
							@Override
							public void run() {
								stopRefresh();
								Toast.makeText(getActivity(), getString(R.string.place_not_found), Toast.LENGTH_SHORT).show();
							}
						});
					} else {
						// cache result and render data
						final JSONObject json = data;
						handler.post(new Runnable() {
							@Override
							public void run() {
								RawCache.writeToCache(getActivity(), city + Utils.CURRENT_CACHE_KEY, temp);
								stopRefresh();
								renderWeather(json);
							}
						});
					}
				}
			}.start();
		}
	}
	
	/**
	 * Setting values of each text views
	 * @param json json response object
	 */
	private void renderWeather(JSONObject json) {
		
		try {
			// simple get of weather data
			String name = json.getString("name").toUpperCase(Locale.US);
			String country = json.getJSONObject("sys").getString("country");
			
			cityField.setText(name + "," + country);
			
			// check API of OpenWeatherMap for details of these values
			long sunrise = json.getJSONObject("sys").getLong("sunrise") * 1000;
			long sunset = json.getJSONObject("sys").getLong("sunset") * 1000;
			c.setTimeInMillis(sunrise);
			Date dateSunrise = c.getTime();
			c.setTimeInMillis(sunset);
			Date dataSunset = c.getTime();
			
			JSONObject details = json.getJSONArray("weather").getJSONObject(0);
			JSONObject main = json.getJSONObject("main");
			
			detailsFields.setText(details.getString("description").toUpperCase(
					Locale.US)
					+ "\n"
					+ getString(R.string.humidity)
					+ " : "
					+ main.getString("humidity")
					+ "%"
					+ "\n"
					+ Utils.HOUR_FORMATTER.format(dateSunrise)
					+ " / "
					+ Utils.HOUR_FORMATTER.format(dataSunset));
			
			String temperature = String.format(Locale.US, "%.2f", main.getDouble("temp"));
			String unit = UserPreference.isCelsius(getActivity()) ? 
							getString(R.string.unit_celsius) : 
							getString(R.string.unit_fahrenheit);
			temperature += " " + unit;
			
			currentTemperatureField.setText(temperature);
			
			String updateOn = DateFormat.getDateTimeInstance().format(
					new Date(json.getLong("dt") * 1000));
			updateField.setText(getString(R.string.last_update) + " : "
					+ updateOn);
			
			// convert id of weather to icon
			weatherIcon.setText(Utils.getWeatherIcon(getActivity(),
					details.getInt("id"), sunrise, sunset));
			
			
		} catch (Exception e) {
			System.out.println("current weather rendering error");
			e.printStackTrace();
			Toast.makeText(getActivity(), getString(R.string.error_rendering), Toast.LENGTH_SHORT).show();
		}
		
		
	}
	
	public void loadLocationWeather(Location location) {
		updateWeatherLocation(location, false);
	}
	
	// similar mechanism for using location
	private void updateWeatherLocation(final Location location, boolean force) {
		startRefresh();
		final String strLocation = Utils.getLatLng(getActivity(), location);
		
		boolean isInCache = RawCache.isInCache(getActivity(), strLocation + Utils.CURRENT_CACHE_KEY);
		boolean hasNetwork = Utils.isConnected(getActivity());
		
		//no network and no cache --> error
		if (!hasNetwork && !isInCache) {
			Toast.makeText(getActivity(), R.string.no_network, Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (isInCache&& !force ) {
			String json = RawCache.fetchFromCache(getActivity(), strLocation + Utils.CURRENT_CACHE_KEY);
			stopRefresh();
			
			JSONObject data = Utils.toJSONObject(json);
			if (data == null) {
				Toast.makeText(getActivity(), getString(R.string.error_parssing), Toast.LENGTH_SHORT).show();
				return;
			}
			
			renderWeather(data);
		}
		else {
			new Thread() {
				public void run() {
					final String temp = WeatherGrabber.loadCurrentWeather(getActivity(), location, UserPreference.getDefaultDegree(getActivity()));
					JSONObject data = Utils.validateResponse(temp);
					
					if (data == null) {
						handler.post(new Runnable() {

							@Override
							public void run() {
								stopRefresh();
								Toast.makeText(getActivity(), getString(R.string.place_not_found), Toast.LENGTH_SHORT).show();
							}
						});
					} else {
						final JSONObject json = data;
						handler.post(new Runnable(){

							@Override
							public void run() {
								RawCache.writeToCache(getActivity(), strLocation + Utils.CURRENT_CACHE_KEY, temp);
								stopRefresh();
								renderWeather(json);
							}
						});
					}
				}
			}.start();
		}
	}


}
