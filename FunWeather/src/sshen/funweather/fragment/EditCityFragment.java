package sshen.funweather.fragment;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import sshen.funweather.R;
import sshen.funweather.dao.CityDao;
import sshen.funweather.grabber.WeatherGrabber;
import sshen.funweather.interfaces.Communicator;
import sshen.funweather.utils.City;
import sshen.funweather.utils.CityFinder;
import sshen.funweather.utils.CityPreference;
import sshen.funweather.utils.UserPreference;
import sshen.funweather.utils.Utils;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class EditCityFragment extends Fragment{
	
	// regular expression pattern
	private static final String CITY_NAME_REGEX = "^[a-zA-Z]+([\\s][a-zA-Z]+)*[\\,][a-zA-Z]*$";
	
	private ImageButton addButton;
	private EditText cityText;
	private Communicator commuicator;
	private ListView savedPlaces;
	private ArrayAdapter<String> citiesAdapter;
	private CityDao cityDao;
	private List<City> cityList;
	private LinearLayout loadingSection;
	private ProgressBar progressBar;
	private TextView loadingMessage;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_edit_city, container, false);
		
		// Initialize UI
		addButton = (ImageButton) rootView.findViewById(R.id.btn_add_city);
		cityText = (EditText) rootView.findViewById(R.id.text_city_name);
		savedPlaces = (ListView) rootView.findViewById(R.id.listview_saved_places);
		loadingSection = (LinearLayout) rootView.findViewById(R.id.loading_section);
		progressBar = (ProgressBar) rootView.findViewById(R.id.add_city_progress_bar);
		loadingMessage = (TextView) rootView.findViewById(R.id.loading_message);
		
		// prepare SQLite
		cityDao = new CityDao(getActivity());
		cityDao.open();
		
		// load saved places
		cityList = cityDao.getAllCities();
		cityList.add(0, CityPreference.DEFAULT_CITY_OBJ);
		citiesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.saved_city_list_item, Utils.toArray(cityList));
		savedPlaces.setAdapter(citiesAdapter);
		
		// add listener to button
		addButton.setOnClickListener(clickListener);
		
		return rootView;
	}
	
	/**
	 * Update list view if new item has been added
	 * @param newList
	 */
	private void updateList(List<City> newList) {
		citiesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.saved_city_list_item, Utils.toArray(newList));
		savedPlaces.setAdapter(citiesAdapter);
		citiesAdapter.notifyDataSetChanged();
	}
	
	/**
	 * Add city to list and database table
	 * @param city
	 */
	private void addCity(City city) {
		cityDao.createCity(city);
		cityList.add(city);
	}
	
	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_add_city) {
				if (containsError(cityText.getText().toString())) {
					return;
				}
				SearchCityTask task = new SearchCityTask();
				task.execute(cityText.getText().toString());
			}
			
		}
		
	};
	
	class SearchCityTask extends AsyncTask<String, Void, City> {

		@Override
		protected void onPreExecute() {
			startSearch();
			// dismiss virtual keyboard
			Utils.dismissKeyBoard(getActivity());
		}
		
		@Override
		protected City doInBackground(String... params) {
			City city = null;
			try {
				// call API to find if city exists
				final String temp = WeatherGrabber.findCity(getActivity(), Utils.capitalize(params[0].trim()));
				Thread.sleep(500);
				
				// check response
				JSONObject data = Utils.validateResponse(temp);
				Thread.sleep(500);
				
				// convert response to city
				city = CityFinder.toCity(data);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			return city; 
		}
		
		@Override
		protected void onPostExecute(City city) {
			if (city != null) {
				if ( isCitySaved(city) ) {
					Toast.makeText(getActivity(), R.string.city_already_cached, Toast.LENGTH_SHORT).show();
				}
				else {
					// city exists, so added to list and database
					addCity(city);
					updateList(cityList);
					commuicator.appendToListView(city);
					Toast.makeText(getActivity(), R.string.new_place_added, Toast.LENGTH_SHORT).show();
				}

			} else {
				Toast.makeText(getActivity(), R.string.place_not_found, Toast.LENGTH_SHORT).show();
			}
			stopSearch();
		}

		
	}
	

	private void startSearch() {
		loadingSection.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.VISIBLE);
		loadingMessage.setVisibility(View.VISIBLE);
	}
	
	private void stopSearch() {
		loadingSection.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);
		loadingMessage.setVisibility(View.GONE);
	}
	
	private boolean containsError(String val) {
		boolean hasError = false;
		int errorIndex = 0;

		// check invalid string
		if ( !Utils.isStringValid(val) ) {
			hasError = true;
			errorIndex = R.string.error_you_must_enter_city;
		}
		else {
			// check input string format
			if (!passRegex(val)) {
				hasError = true;
				errorIndex = R.string.error_wrong_input_format;
			}
		}

		// check Internet connectivity
		if (!Utils.isConnected(getActivity())) {
			hasError = true;
			errorIndex = R.string.no_network;
		}

		if (hasError && errorIndex != 0) {
			Toast.makeText(getActivity(), errorIndex, Toast.LENGTH_SHORT).show();
		}
		
		return hasError;
	}
	
	/**
	 * Check if a input string match the City,Country pattern
	 * @param val input
	 * @return true if val matches, false otherwise 
	 */
	private boolean passRegex(String val) {
		Pattern p = Pattern.compile(CITY_NAME_REGEX);
		Matcher m = p.matcher(val);
		return m.matches();
	}
	
	/**
	 * Check if user is trying to duplicated city
	 * @param city City
	 * @return true if city has been added previously, false otherwise
	 */
	private boolean isCitySaved(City city) {
		for (City c : cityList) {
			if (c.getId() == city.getId())
				return true;
		}
		return false;
	}
	
	
	@Override
	public void onResume() {
		cityDao.open();
		super.onResume();
	};
	
	@Override
	public void onPause() {
		cityDao.close();
		super.onPause();
	}
	
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		commuicator = (Communicator) activity;
	}


}
