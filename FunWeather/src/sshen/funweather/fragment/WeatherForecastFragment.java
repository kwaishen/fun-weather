package sshen.funweather.fragment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import sshen.funweather.R;
import sshen.funweather.adapter.ForecastGridAdapter;
import sshen.funweather.cache.RawCache;
import sshen.funweather.grabber.WeatherGrabber;
import sshen.funweather.model.ForecastGridItem;
import sshen.funweather.utils.City;
import sshen.funweather.utils.UserPreference;
import sshen.funweather.utils.Utils;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


public class WeatherForecastFragment extends Fragment{
	
	private static final String TAG = "WeatherForecastFragment";
	private GridView mGridView;
	private ForecastGridAdapter adapter;
	private ArrayList<ForecastGridItem> list;
	private TextView refreshIcon, refreshField;

	private City city;
	private Location location;
	private Handler handler;
	
	
	public WeatherForecastFragment(City city) {
		this.city = city;
	}
	
	public WeatherForecastFragment(Location location) {
		this.location = location;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handler = new Handler();
	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_weather_forecast, container, false);
		mGridView = (GridView) rootView.findViewById(R.id.forecast_gridview);
		refreshField = (TextView) rootView.findViewById(R.id.label_refresh_forecast);
		refreshIcon = (TextView) rootView.findViewById(R.id.forecast_refresh_icon);
		refreshIcon.setTypeface(Utils.getWeatherFont(getActivity()));
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (this.city != null) {
			updateWeatherCity(city.getFullName(), false);
		} else if (this.location != null) {
			updateWeatherLocation(location, false);
		} else {
			// do nothing
		}
	}
	

	/**
	 * Force refresh weather forecast
	 */
	public void refresh() {
		if (this.city != null) {
			updateWeatherCity(city.getFullName(), true);
		}
		else if (this.location != null) {
			updateWeatherLocation(location, true);
		}
		else {
			Toast.makeText(getActivity(), R.string.error_refresh_city, Toast.LENGTH_SHORT).show();
		}
	}
	
	private void startRefresh() {
		refreshField.setVisibility(View.VISIBLE);
		refreshIcon.setVisibility(View.VISIBLE);
		mGridView.setVisibility(View.GONE);

	}
	
	private void stopRefresh() {
		refreshField.setVisibility(View.GONE);
		refreshIcon.setVisibility(View.GONE);
		mGridView.setVisibility(View.VISIBLE);
	}
	
	private void updateWeatherCity(final String city, boolean force) {
		startRefresh();
		
		// check from cache
		boolean isInCache = RawCache.isInCache(getActivity(), city + Utils.FORECAST_CACHE_KEY);
		boolean hasNetwork = Utils.isConnected(getActivity());
		
		// no network and no cache --> error
		if (!hasNetwork && !isInCache) {
			Toast.makeText(getActivity(), R.string.no_network, Toast.LENGTH_SHORT).show();
			return;
		}

		if (isInCache && !force) {
			String json = RawCache.fetchFromCache(getActivity(), city + Utils.FORECAST_CACHE_KEY);
			stopRefresh();

			JSONObject data = Utils.toJSONObject(json);
			if (data == null) {
				Toast.makeText(getActivity(), R.string.error_parssing, Toast.LENGTH_SHORT).show();
				return;
			}

			renderWeather(data);

		} else {
			// get from network in separate Thread
			new Thread() {
				public void run() {
					final String temp = WeatherGrabber.loadForecast(getActivity(), city, UserPreference.getDefaultDegree(getActivity()));
					JSONObject data = Utils.validateResponse(temp);
					
					if (data == null) {
						// error finding city
						handler.post(new Runnable() {
							@Override
							public void run() {
								stopRefresh();
								Toast.makeText(getActivity(), R.string.place_not_found, Toast.LENGTH_SHORT).show();
							}
						});
					} else {
						// cache result and render data
						final JSONObject json = data;
						handler.post(new Runnable() {
							@Override
							public void run() {
								RawCache.writeToCache(getActivity(), city + Utils.FORECAST_CACHE_KEY, temp);
								stopRefresh();
								renderWeather(json);
							}
						});
					}
				}
			}.start();
		}
	}
	
	private void updateWeatherLocation(final Location location, boolean force) {
		startRefresh();
		
		final String strLocation = Utils.getLatLng(getActivity(), location);

		boolean isInCache = RawCache.isInCache(getActivity(), strLocation + Utils.FORECAST_CACHE_KEY);
		boolean hasNetwork = Utils.isConnected(getActivity());

		//no network and no cache --> error
		if (!hasNetwork && !isInCache) {
			Toast.makeText(getActivity(), R.string.no_network, Toast.LENGTH_SHORT).show();
			return;
		}

		if (isInCache&& !force ) {
			String json = RawCache.fetchFromCache(getActivity(), strLocation + Utils.FORECAST_CACHE_KEY);
			stopRefresh();

			JSONObject data = Utils.toJSONObject(json);
			if (data == null) {
				Toast.makeText(getActivity(), R.string.error_parssing, Toast.LENGTH_SHORT).show();
				return;
			}

			renderWeather(data);
		}
		else {
			new Thread() {
				public void run() {
					final String temp = WeatherGrabber.loadForecast(getActivity(), location, UserPreference.getDefaultDegree(getActivity()));
					JSONObject data = Utils.validateResponse(temp);
					
					if (data == null) {
						handler.post(new Runnable() {

							@Override
							public void run() {
								stopRefresh();
								Toast.makeText(getActivity(), getString(R.string.place_not_found), Toast.LENGTH_SHORT).show();
							}
						});
					} else {
						final JSONObject json = data;
						handler.post(new Runnable(){

							@Override
							public void run() {
								RawCache.writeToCache(getActivity(), strLocation + Utils.FORECAST_CACHE_KEY, temp);
								stopRefresh();
								renderWeather(json);
							}
						});
					}
				}
			}.start();
		}
		
	}
	
	private void renderWeather(JSONObject json) {
		
		try {
			list = new ArrayList<ForecastGridItem>();
			
			JSONArray array = json.getJSONArray("list");
			
			for (int i=0; i< array.length(); i++) {
				JSONObject listItem = array.getJSONObject(i);
				String date = DateFormat.getDateInstance().format(
						new Date(listItem.getLong("dt") * 1000));
				
				// temperature part
				JSONObject temp = listItem.getJSONObject("temp");
				String min = temp.getString("min");
				String max = temp.getString("max");
				StringBuffer sb = new StringBuffer();
				String unit = UserPreference.isCelsius(getActivity()) ? 
							getString(R.string.unit_celsius) : 
							getString(R.string.unit_fahrenheit);
				sb.append(min).append(" ").append(unit).append(" / ").append(max).append(unit);
				String temperature = sb.toString();

				
				// weather part
				JSONObject weather = listItem.getJSONArray("weather").getJSONObject(0);
				int iconId = weather.getInt("id");
				String iconString = getString(Utils.getSimpleWeatherIcon(getActivity(), iconId)) ;
				String description = weather.getString("description");
				
				// make a grid item
				ForecastGridItem item = new ForecastGridItem(date, description, temperature, iconString);
				// add item to grid list
				list.add(item);
				
			}
			
			// setting adapters
			adapter = new ForecastGridAdapter(getActivity(), list);
			mGridView.setAdapter(adapter);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getActivity(), R.string.error_rendering, Toast.LENGTH_SHORT).show();
		}
		
	}
}
