package sshen.funweather.cache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import android.content.Context;
import android.util.Log;

public class RawCache {
	
	public static final String PREFIX = "Funny_Weather_SShen";
	public static final String SEPARATOR = "_";
	public static final long RETENTION_TIME = 1000 * 60 * 10; // 10 minutes
	
	
	public static File getRoot(Context ctx) {
		return ctx.getFilesDir();
	}
	
	/**
	 * Generate a cache key by type
	 */
	public static String generateKey(String type) {
		return PREFIX + SEPARATOR + type;
	}
	
	/**
	 * Check if a file is in internal cache folder
	 * @param ctx Context
	 * @param type File Type
	 * @return true if the file is kept in cache, false otherwise
	 */
	public static boolean isInCache(Context ctx, String type) {
		boolean isInCache = false;
		if (ctx != null) {
			File file = new File(getRoot(ctx), generateKey(type));
			if (file != null && file.exists()) {
				if (System.currentTimeMillis() - file.lastModified() < RETENTION_TIME) {
					isInCache = true;
				} else {
					// delete cache file
					try {
						file.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		return isInCache;
	}
	
	
	/**
	 * Cache data
	 */
	public static void writeToCache(Context ctx, String type, String data) {
		if (ctx != null && data != null && !"".equals(data)) {
			File file = null;
			FileWriter writer = null;
			
			try {
				file = new File(getRoot(ctx), generateKey(type));
				writer = new FileWriter(file);
				writer.write(data);
				
			} catch (Exception exc) {
				exc.printStackTrace();
			} finally {
				if (writer != null) {
					try { writer.close(); } catch (Exception e) { e.printStackTrace();	}
				}
			}
			
		}
	}

	/**
	 * Read data from cache 
	 * @return cache result in String 
	 */
	public static String fetchFromCache(Context ctx, String type) {
		String data = null;
		
		if (ctx != null) {
			File file = null;
			FileReader reader = null;
			BufferedReader buf = null;
			
			try {
				file = new File(getRoot(ctx), generateKey(type));
				reader = new FileReader(file);
				buf = new BufferedReader(reader);
				data = buf.readLine();
				
			} catch (Exception e) {
				if (reader != null) {
					try {
						reader.close();
					} catch (Exception exc) {
						exc.printStackTrace();
					}
				}
				
				if (buf != null) {
					try {
						buf.close();
					} catch (Exception exc) {
						exc.printStackTrace();
					}
				}
			}
			
		}
		
		return data;
	}
	
}
