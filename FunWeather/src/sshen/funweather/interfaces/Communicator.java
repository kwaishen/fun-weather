package sshen.funweather.interfaces;

import sshen.funweather.utils.City;

/**
 * Interface that help decoupling between MainActivity and Fragments
 * @author Steve
 *
 */
public interface Communicator {
	
	public void appendToListView(City city); 
	public void refresh();
}
