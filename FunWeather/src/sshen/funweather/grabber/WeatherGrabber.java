package sshen.funweather.grabber;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;

import sshen.funweather.utils.Utils;
import sshen.funweather.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Log;

public class WeatherGrabber {
	
	private static final String TAG = "WeatherGrabber";
	private static final String CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=%s";
	private static final String CURRENT_WEATHER_LOCATION_URL = "http://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&units=%s";
	private static final String WEATHER_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?q=%s&cnt=%s&units=%s";
	private static final String WEATHER_FORECAST_LOCATION_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=%s&lon=%s&cnt=%s&units=%s";
	private static final String CNT = "7";
	private static final DecimalFormat DF = new DecimalFormat("0.0000"); 
	private static final String FIND_CITY_URL = "http://api.openweathermap.org/data/2.5/find?q=%s";//&type=like/accurate
	
	
	// Find City
	public static String findCity(Context context, String city) {
		String str = String.format(FIND_CITY_URL, city);
		return load(context, str);
	}
	
	// City
	public static String loadCurrentWeather(Context context, String city, String degree) {
		String str = String.format(CURRENT_WEATHER_URL, city, degree);
		return load(context, str);
	}
	
	// Location
	public static String loadCurrentWeather(Context context, Location location, String degree) {
		double lat = location.getLatitude();
		double lon = location.getLongitude();
		String str = String.format(CURRENT_WEATHER_LOCATION_URL, DF.format(lat), DF.format(lon), degree );
		return load(context, str);
	}
	
	//======= Load Forecast ===================
	public static String loadForecast(Context context, String city, String degree) {

		String str = String.format(WEATHER_FORECAST_URL, city, CNT, degree);
		return load(context, str);
	}

	public static String loadForecast(Context context, Location location, String degree) {
		double lat = location.getLatitude();
		double lon = location.getLongitude();
		String str = String.format(WEATHER_FORECAST_LOCATION_URL, DF.format(lat), DF.format(lon), CNT, degree );
		
		return load(context,str);
	}
	
	private static String load(Context context, String strUrl) {
		BufferedReader reader = null;
		InputStream is = null;
		HttpURLConnection con = null;
		
		try{
			URL url = new URL(strUrl);
			con = (HttpURLConnection) url.openConnection(); 
			
			// append API key
			con.addRequestProperty("x-api-key", 
                    context.getString(R.string.owm_api_key));
			
			is = con.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is));
			
			// ready to read 
			StringBuffer json = new StringBuffer();
			String temp = "";
			
			while ((temp = reader.readLine()) != null) {
				json.append(temp).append("\n");
			}
			return json.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try { reader.close(); } catch (Exception e) {e.printStackTrace();}
			}
			if (is != null) {
				try { is.close(); } catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) { 
				try { con.disconnect(); } catch (Exception e) {e.printStackTrace();}
			}
		}
		
		return null;
	}
	

}
